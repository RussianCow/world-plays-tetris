(function() {
    var KEY_NAMES = {
        37: 'left',
        38: 'up',
        39: 'right',
        40: 'down'
    }


    function writeMessage(message) {
        var p = document.getElementById('message')
        p.textContent = message
        document.body.appendChild(p)
    }


    var ws = new WebSocket('ws://sasha-linux-tetris.local/feed')

    ws.onopen = function(_) {
        ws.onmessage = function(event) {
            writeMessage(event.data)
        }

        function sendMessage(msg) {
            ws.send(msg)
        }

        window.addEventListener('keydown', function(e) {
            var keyName = KEY_NAMES[e.keyCode]
            if (keyName) {
                ws.send(keyName)
            }
        })
    }

    ws.onerror = function(_) {
        writeMessage("Error")
    }

    ws.onclose = function(_) {
        writeMessage("Close")
    }
})()
