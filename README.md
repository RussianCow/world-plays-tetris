# World Plays Tetris

It's like Twitch Plays Pokémon, only Tetris, and web-based—many people playing the same game of Tetris in realtime. This is a personal experiment in building a scalable, realtime web app, and also a chance for me to learn Elixir and the Erlang platform. The goal is to eventually also throw Kafka and potentially something like Cassandra into the mix, but I'm getting ahead of myself.

As I work on this, I will update this readme with progress and high-level design decisions.
