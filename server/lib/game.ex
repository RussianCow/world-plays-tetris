defmodule Tetris.Game do
  @field_size [width: 10, height: 22]

  def start(parent_pid) do
    seed = :os.system_time
    shapes_pid = spawn fn -> Tetris.Shapes.start_loop(seed) end
    field = for _ <- 1..@field_size[:height], do: (for _ <- 1..@field_size[:width], do: 0)
    current_shape = new_shape(shapes_pid)
    next_shape = new_shape(shapes_pid)
    loop(parent_pid, shapes_pid, seed, field, current_shape, next_shape)
  end

  defp loop(parent_pid, shapes_pid, seed, field, current_shape, next_shape) do
    send(parent_pid, {self, :field, field})
    move = receive_move
    new_field = apply_move(move, field)
    loop(parent_pid, shapes_pid, seed, new_field, current_shape, next_shape)
  end

  defp receive_move do
    receive do
      move -> move
    after
      1_500 -> :down
    end
  end

  defp apply_move(move, field) do
    field
  end

  defp new_shape(shapes_pid) do
    send(shapes_pid, {:next_shape, self()})
    receive do shape -> shape end
  end
end
