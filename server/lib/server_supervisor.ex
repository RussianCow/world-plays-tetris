defmodule Tetris.ServerSupervisor do
  def run(parent_id) do
    s = self
    server_pid = spawn fn -> Tetris.Server.run(s) end
    receive_messages(parent_id, server_pid, [])
  end

  defp receive_messages(parent_id, server_pid, output_pids) do
    new_pids =
      receive do
        {:new_client, client} ->
          output_pid = handle_client(client)
          output_pids ++ [output_pid]
        {:field, field} ->
          send_field(output_pids, field)
          output_pids
        {:move, move} ->
          send(parent_id, {:move, move})
      end
    receive_messages(parent_id, server_pid, new_pids)
  end

  defp handle_client(client) do
    output_pid = spawn fn -> Tetris.Output.handle_client(client) end
    spawn fn -> Tetris.Input.handle_client(self, client) end
    output_pid
  end

  defp send_field(output_pids, field) do
    for pid <- output_pids, do: send(pid, field)
  end
end
