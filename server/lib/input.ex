defmodule Tetris.Input do
  def handle_client(parent_pid, client) do
    msg = client |> Socket.Web.recv!
    handle_message(parent_pid, client, msg)
    handle_client(parent_pid, client)
  end

  defp handle_message(parent_pid, client, msg) do
    case msg do
      {:text, text} ->
        if text in ["left", "right", "up", "down"] do
          atom = String.to_atom(text)
          send(parent_pid, {:move, atom})
        end
      {:close, _, _} ->
        client |> Socket.Web.close
      {:pong, _} ->
        :ok
      _ ->
        IO.inspect(msg)
    end
  end
end
