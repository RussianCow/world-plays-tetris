defmodule Tetris.Output do
  def handle_client(client) do
    receive do
      msg ->
        json = Poison.encode!(msg)
        client |> Socket.Web.send!({:text, json})
    after
      4_000 ->
        client |> Socket.Web.ping!
    end
    handle_client(client)
  end
end
