defmodule Tetris.Server do
  def run(parent_id) do
    server = Socket.Web.listen! 8001
    receive_connections(parent_id, server)
  end

  defp receive_connections(parent_pid, server) do
  	client = server |> Socket.Web.accept!
    client |> Socket.Web.accept!
    send(parent_pid, {:new_client, client})
  	IO.puts("Connection!")
    receive_connections(parent_pid, server)
  end
end
