defmodule Tetris.Shapes do
  @shapes [
    [[1]],
    [[1, 1],
     [1, 1]]
  ]

  def start_loop(seed) do
    :random.seed(seed)
    loop
  end

  defp loop do
    receive do
      {:next_shape, caller} -> send(caller, next_shape)
    end
    loop
  end

  defp next_shape do
    Enum.random(@shapes)
  end
end
