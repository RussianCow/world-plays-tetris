defmodule Tetris do
  use Application

  def start(_type, _args) do
    s = self
  	server_pid = spawn_link fn -> Tetris.ServerSupervisor.run(s) end
    game_pid = spawn_link fn -> Tetris.Game.start(s) end

    IO.puts("Starting server...")
    receive_messages(server_pid, game_pid)
  	{:ok, self()}
  end

  defp receive_messages(server_pid, game_pid) do
    receive do
      {^game_pid, :field, field} ->
        send(server_pid, {:field, field})
    end
    receive_messages(server_pid, game_pid)
  end
end
