defmodule Tetris.Mixfile do
  use Mix.Project

  def project do
    [app: :tetris,
     version: "0.0.1",
     elixir: "~> 1.2",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     deps: deps]
  end

  def application do
    [applications: [:logger],
     mod: {Tetris, []}]
  end

  defp deps do
    [{:socket, "~> 0.3"},
     {:poison, "~> 2.0"}]
  end
end
