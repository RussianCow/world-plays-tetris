#!/usr/bin/env bash

echo "+++ Installing nvm and Node.js +++"
# This gets rid of some npm compile errors when installing modules
sudo apt-get install -y -q node-gyp

# Latest NVM install instructions at:
# https://github.com/creationix/nvm
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.30.1/install.sh | bash

# Get access to the "nvm" command
. ~/.nvm/nvm.sh

# Install latest stable version of node.js
nvm install 5.4
nvm alias default 5.4
