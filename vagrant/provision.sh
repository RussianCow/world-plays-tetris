#!/usr/bin/env bash

echo "+++ Installing basic dependencies +++"
    apt-get update -q
    apt-get upgrade -y -q
    apt-get install -y -q vim git curl openssl libssl-dev

echo "+++ Installing nginx +++"
    apt-get install -y -q nginx
    rm /etc/nginx/sites-enabled/default
    ln -s /etc/nginx/sites-available/tetris /etc/nginx/sites-enabled/tetris
    ln -s /vagrant/vagrant/nginx-config /etc/nginx/sites-available/tetris
    nginx -s stop

    # Generate self-signed SSL certificate
    # http://superuser.com/questions/226192/openssl-without-prompt
    mkdir /etc/nginx/ssl
    openssl req -x509 -nodes -days 365 -newkey rsa:2048 \
        -subj "/C=US/ST=OR/L=Portland/O=PNDLM/CN=localdev" -keyout /etc/nginx/ssl/nginx.key \
        -out /etc/nginx/ssl/nginx.crt

echo "+++ Installing Erlang and Elixir +++"
    wget https://packages.erlang-solutions.com/erlang-solutions_1.0_all.deb && \
        dpkg -i erlang-solutions_1.0_all.deb
    rm erlang-solutions_1.0_all.deb
    apt-get update -q
    apt-get install -y -q esl-erlang elixir

echo "+++ Installing Cassandra +++"
    add-apt-repository ppa:webupd8team/java
    echo "deb http://www.apache.org/dist/cassandra/debian 30x main" | \
        tee -a /etc/apt/sources.list.d/cassandra.sources.list
    echo "deb-src http://www.apache.org/dist/cassandra/debian 30x main" | \
        tee -a /etc/apt/sources.list.d/cassandra.sources.list
    gpg --keyserver pgp.mit.edu --recv-keys F758CE318D77295D
    gpg --export --armor F758CE318D77295D | apt-key add -
    gpg --keyserver pgp.mit.edu --recv-keys 2B5C1B00
    gpg --export --armor 2B5C1B00 | apt-key add -
    gpg --keyserver pgp.mit.edu --recv-keys 0353B12C
    gpg --export --armor 0353B12C | apt-key add -
    apt-get update -q
    echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | \
        /usr/bin/debconf-set-selections
    apt-get install -y -q oracle-java8-installer
    apt-get install -y -q oracle-java8-set-default
    apt-get install -y -q cassandra

echo "+++ Installing Kafka +++"
    echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections
    apt-get install -y -q zookeeperd
    export KAFKA_VERSION="0.9.0.0"
    wget http://www.motorlogy.com/apache/kafka/$KAFKA_VERSION/kafka_2.11-$KAFKA_VERSION.tgz
    tar xf kafka_2.11-$KAFKA_VERSION.tgz
    rm kafka_2.11-$KAFKA_VERSION.tgz

echo "+++ Setting up mDNS +++"
    apt-get install -y -q avahi-daemon avahi-discover libnss-mdns
